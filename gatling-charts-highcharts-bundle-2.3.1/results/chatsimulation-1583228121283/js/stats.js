var stats = {
    type: "GROUP",
name: "Global Information",
path: "",
pathFormatted: "group_missing-name-b06d1",
stats: {
    "name": "Global Information",
    "numberOfRequests": {
        "total": "140",
        "ok": "48",
        "ko": "92"
    },
    "minResponseTime": {
        "total": "20",
        "ok": "20",
        "ko": "6584"
    },
    "maxResponseTime": {
        "total": "58740",
        "ok": "29277",
        "ko": "58740"
    },
    "meanResponseTime": {
        "total": "20739",
        "ok": "10486",
        "ko": "26088"
    },
    "standardDeviation": {
        "total": "15080",
        "ok": "9623",
        "ko": "14638"
    },
    "percentiles1": {
        "total": "15962",
        "ok": "7051",
        "ko": "36858"
    },
    "percentiles2": {
        "total": "36982",
        "ok": "15974",
        "ko": "37102"
    },
    "percentiles3": {
        "total": "37306",
        "ok": "29231",
        "ko": "37408"
    },
    "percentiles4": {
        "total": "37509",
        "ok": "29273",
        "ko": "39451"
    },
    "group1": {
        "name": "t < 800 ms",
        "count": 3,
        "percentage": 2
    },
    "group2": {
        "name": "800 ms < t < 1200 ms",
        "count": 0,
        "percentage": 0
    },
    "group3": {
        "name": "t > 1200 ms",
        "count": 45,
        "percentage": 32
    },
    "group4": {
        "name": "failed",
        "count": 92,
        "percentage": 66
    },
    "meanNumberOfRequestsPerSecond": {
        "total": "1.308",
        "ok": "0.449",
        "ko": "0.86"
    }
},
contents: {
"req_login-99dea": {
        type: "REQUEST",
        name: "Login",
path: "Login",
pathFormatted: "req_login-99dea",
stats: {
    "name": "Login",
    "numberOfRequests": {
        "total": "100",
        "ok": "8",
        "ko": "92"
    },
    "minResponseTime": {
        "total": "6556",
        "ok": "6556",
        "ko": "6584"
    },
    "maxResponseTime": {
        "total": "58740",
        "ok": "7659",
        "ko": "58740"
    },
    "meanResponseTime": {
        "total": "24580",
        "ok": "7230",
        "ko": "26088"
    },
    "standardDeviation": {
        "total": "14944",
        "ok": "447",
        "ko": "14638"
    },
    "percentiles1": {
        "total": "36764",
        "ok": "7524",
        "ko": "36858"
    },
    "percentiles2": {
        "total": "37092",
        "ok": "7570",
        "ko": "37102"
    },
    "percentiles3": {
        "total": "37401",
        "ok": "7639",
        "ko": "37408"
    },
    "percentiles4": {
        "total": "37755",
        "ok": "7655",
        "ko": "39451"
    },
    "group1": {
        "name": "t < 800 ms",
        "count": 0,
        "percentage": 0
    },
    "group2": {
        "name": "800 ms < t < 1200 ms",
        "count": 0,
        "percentage": 0
    },
    "group3": {
        "name": "t > 1200 ms",
        "count": 8,
        "percentage": 8
    },
    "group4": {
        "name": "failed",
        "count": 92,
        "percentage": 92
    },
    "meanNumberOfRequestsPerSecond": {
        "total": "0.935",
        "ok": "0.075",
        "ko": "0.86"
    }
}
    },"req_fetch-users-69b29": {
        type: "REQUEST",
        name: "Fetch users",
path: "Fetch users",
pathFormatted: "req_fetch-users-69b29",
stats: {
    "name": "Fetch users",
    "numberOfRequests": {
        "total": "8",
        "ok": "8",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "29118",
        "ok": "29118",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "29277",
        "ok": "29277",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "29207",
        "ok": "29207",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "51",
        "ok": "51",
        "ko": "-"
    },
    "percentiles1": {
        "total": "29204",
        "ok": "29204",
        "ko": "-"
    },
    "percentiles2": {
        "total": "29249",
        "ok": "29249",
        "ko": "-"
    },
    "percentiles3": {
        "total": "29274",
        "ok": "29274",
        "ko": "-"
    },
    "percentiles4": {
        "total": "29276",
        "ok": "29276",
        "ko": "-"
    },
    "group1": {
        "name": "t < 800 ms",
        "count": 0,
        "percentage": 0
    },
    "group2": {
        "name": "800 ms < t < 1200 ms",
        "count": 0,
        "percentage": 0
    },
    "group3": {
        "name": "t > 1200 ms",
        "count": 8,
        "percentage": 100
    },
    "group4": {
        "name": "failed",
        "count": 0,
        "percentage": 0
    },
    "meanNumberOfRequestsPerSecond": {
        "total": "0.075",
        "ok": "0.075",
        "ko": "-"
    }
}
    },"req_click-on-ola-2dd0a": {
        type: "REQUEST",
        name: "Click on Ola",
path: "Click on Ola",
pathFormatted: "req_click-on-ola-2dd0a",
stats: {
    "name": "Click on Ola",
    "numberOfRequests": {
        "total": "8",
        "ok": "8",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "15912",
        "ok": "15912",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "16002",
        "ok": "16002",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "15971",
        "ok": "15971",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "29",
        "ok": "29",
        "ko": "-"
    },
    "percentiles1": {
        "total": "15980",
        "ok": "15980",
        "ko": "-"
    },
    "percentiles2": {
        "total": "15993",
        "ok": "15993",
        "ko": "-"
    },
    "percentiles3": {
        "total": "16000",
        "ok": "16000",
        "ko": "-"
    },
    "percentiles4": {
        "total": "16002",
        "ok": "16002",
        "ko": "-"
    },
    "group1": {
        "name": "t < 800 ms",
        "count": 0,
        "percentage": 0
    },
    "group2": {
        "name": "800 ms < t < 1200 ms",
        "count": 0,
        "percentage": 0
    },
    "group3": {
        "name": "t > 1200 ms",
        "count": 8,
        "percentage": 100
    },
    "group4": {
        "name": "failed",
        "count": 0,
        "percentage": 0
    },
    "meanNumberOfRequestsPerSecond": {
        "total": "0.075",
        "ok": "0.075",
        "ko": "-"
    }
}
    },"req_send-msg-to-ola-0383d": {
        type: "REQUEST",
        name: "Send msg to Ola",
path: "Send msg to Ola",
pathFormatted: "req_send-msg-to-ola-0383d",
stats: {
    "name": "Send msg to Ola",
    "numberOfRequests": {
        "total": "8",
        "ok": "8",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "1586",
        "ok": "1586",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "1700",
        "ok": "1700",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "1630",
        "ok": "1630",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "37",
        "ok": "37",
        "ko": "-"
    },
    "percentiles1": {
        "total": "1630",
        "ok": "1630",
        "ko": "-"
    },
    "percentiles2": {
        "total": "1651",
        "ok": "1651",
        "ko": "-"
    },
    "percentiles3": {
        "total": "1683",
        "ok": "1683",
        "ko": "-"
    },
    "percentiles4": {
        "total": "1697",
        "ok": "1697",
        "ko": "-"
    },
    "group1": {
        "name": "t < 800 ms",
        "count": 0,
        "percentage": 0
    },
    "group2": {
        "name": "800 ms < t < 1200 ms",
        "count": 0,
        "percentage": 0
    },
    "group3": {
        "name": "t > 1200 ms",
        "count": 8,
        "percentage": 100
    },
    "group4": {
        "name": "failed",
        "count": 0,
        "percentage": 0
    },
    "meanNumberOfRequestsPerSecond": {
        "total": "0.075",
        "ok": "0.075",
        "ko": "-"
    }
}
    },"req_click-on-kari-dd999": {
        type: "REQUEST",
        name: "Click on kari",
path: "Click on kari",
pathFormatted: "req_click-on-kari-dd999",
stats: {
    "name": "Click on kari",
    "numberOfRequests": {
        "total": "8",
        "ok": "8",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "20",
        "ok": "20",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "5213",
        "ok": "5213",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "3242",
        "ok": "3242",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "2482",
        "ok": "2482",
        "ko": "-"
    },
    "percentiles1": {
        "total": "5107",
        "ok": "5107",
        "ko": "-"
    },
    "percentiles2": {
        "total": "5192",
        "ok": "5192",
        "ko": "-"
    },
    "percentiles3": {
        "total": "5211",
        "ok": "5211",
        "ko": "-"
    },
    "percentiles4": {
        "total": "5213",
        "ok": "5213",
        "ko": "-"
    },
    "group1": {
        "name": "t < 800 ms",
        "count": 3,
        "percentage": 38
    },
    "group2": {
        "name": "800 ms < t < 1200 ms",
        "count": 0,
        "percentage": 0
    },
    "group3": {
        "name": "t > 1200 ms",
        "count": 5,
        "percentage": 63
    },
    "group4": {
        "name": "failed",
        "count": 0,
        "percentage": 0
    },
    "meanNumberOfRequestsPerSecond": {
        "total": "0.075",
        "ok": "0.075",
        "ko": "-"
    }
}
    },"req_send-msg-to-kar-99701": {
        type: "REQUEST",
        name: "Send msg to Kari",
path: "Send msg to Kari",
pathFormatted: "req_send-msg-to-kar-99701",
stats: {
    "name": "Send msg to Kari",
    "numberOfRequests": {
        "total": "8",
        "ok": "8",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "3120",
        "ok": "3120",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "7321",
        "ok": "7321",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "5634",
        "ok": "5634",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "1910",
        "ok": "1910",
        "ko": "-"
    },
    "percentiles1": {
        "total": "7036",
        "ok": "7036",
        "ko": "-"
    },
    "percentiles2": {
        "total": "7071",
        "ok": "7071",
        "ko": "-"
    },
    "percentiles3": {
        "total": "7247",
        "ok": "7247",
        "ko": "-"
    },
    "percentiles4": {
        "total": "7306",
        "ok": "7306",
        "ko": "-"
    },
    "group1": {
        "name": "t < 800 ms",
        "count": 0,
        "percentage": 0
    },
    "group2": {
        "name": "800 ms < t < 1200 ms",
        "count": 0,
        "percentage": 0
    },
    "group3": {
        "name": "t > 1200 ms",
        "count": 8,
        "percentage": 100
    },
    "group4": {
        "name": "failed",
        "count": 0,
        "percentage": 0
    },
    "meanNumberOfRequestsPerSecond": {
        "total": "0.075",
        "ok": "0.075",
        "ko": "-"
    }
}
    }
}

}

function fillStats(stat){
    $("#numberOfRequests").append(stat.numberOfRequests.total);
    $("#numberOfRequestsOK").append(stat.numberOfRequests.ok);
    $("#numberOfRequestsKO").append(stat.numberOfRequests.ko);

    $("#minResponseTime").append(stat.minResponseTime.total);
    $("#minResponseTimeOK").append(stat.minResponseTime.ok);
    $("#minResponseTimeKO").append(stat.minResponseTime.ko);

    $("#maxResponseTime").append(stat.maxResponseTime.total);
    $("#maxResponseTimeOK").append(stat.maxResponseTime.ok);
    $("#maxResponseTimeKO").append(stat.maxResponseTime.ko);

    $("#meanResponseTime").append(stat.meanResponseTime.total);
    $("#meanResponseTimeOK").append(stat.meanResponseTime.ok);
    $("#meanResponseTimeKO").append(stat.meanResponseTime.ko);

    $("#standardDeviation").append(stat.standardDeviation.total);
    $("#standardDeviationOK").append(stat.standardDeviation.ok);
    $("#standardDeviationKO").append(stat.standardDeviation.ko);

    $("#percentiles1").append(stat.percentiles1.total);
    $("#percentiles1OK").append(stat.percentiles1.ok);
    $("#percentiles1KO").append(stat.percentiles1.ko);

    $("#percentiles2").append(stat.percentiles2.total);
    $("#percentiles2OK").append(stat.percentiles2.ok);
    $("#percentiles2KO").append(stat.percentiles2.ko);

    $("#percentiles3").append(stat.percentiles3.total);
    $("#percentiles3OK").append(stat.percentiles3.ok);
    $("#percentiles3KO").append(stat.percentiles3.ko);

    $("#percentiles4").append(stat.percentiles4.total);
    $("#percentiles4OK").append(stat.percentiles4.ok);
    $("#percentiles4KO").append(stat.percentiles4.ko);

    $("#meanNumberOfRequestsPerSecond").append(stat.meanNumberOfRequestsPerSecond.total);
    $("#meanNumberOfRequestsPerSecondOK").append(stat.meanNumberOfRequestsPerSecond.ok);
    $("#meanNumberOfRequestsPerSecondKO").append(stat.meanNumberOfRequestsPerSecond.ko);
}
